<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>
    <script>
        jQuery(document).ready(function () {
            let oldString = jQuery(".category-ak2019 a.page-number").attr("href");
            let newString = oldString.replace("&type_aws=true", "");
            jQuery(".category-ak2019 a.page-number").attr("href", newString);
        });
    </script>

    <div id="content" class="blog-wrapper blog-archive page-wrapper">
        <?php if (function_exists("mabagroup_go_has_customer_purchased_calendar_ticket") && mabagroup_go_has_customer_purchased_calendar_ticket()) : ?>
            <?php get_template_part('template-parts/posts/layout', get_theme_mod('blog_layout', 'right-sidebar')); ?>
        <?php else: ?>
            <div class="row align-center">
                <div class="large-10 col">
                    <span style="color: white; background-color: red; padding: 1em;">Um diese Kategorie zu lesen benötigst Du ein Adventkalender-Ticket.</span>
                </div>
            </div>
        <?php endif; ?>
    </div><!-- .page-wrapper .blog-wrapper -->


<?php get_footer(); ?>